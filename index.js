/** @format */
import React, { Component } from "react";
import { AppRegistry } from "react-native";
import App from "./App";
import { name as appName } from "./app.json";

import { createUploadLink } from "apollo-upload-client";
import { ApolloClient } from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import { InMemoryCache } from "apollo-cache-inmemory";

const link = createUploadLink({ uri: "http://192.168.22.17:4000/" });

const client = new ApolloClient({
  link,
  cache: new InMemoryCache()
});

class Apollo extends React.Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <App />
      </ApolloProvider>
    );
  }
}

AppRegistry.registerComponent(appName, () => Apollo);

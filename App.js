/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  Text,
  View,
  Image,
  FlatList
} from "react-native";
import ImagePicker from "react-native-image-crop-picker";
import Modal from "react-native-modal";

import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { ReactNativeFile } from "apollo-upload-client";

console.disableYellowBox = true;

const uploadFileMutation = gql`
  mutation($file: [Upload!]!) {
    uploadFile(file: $file)
  }
`;

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      imageData: []
    };
  }

  _onModal = () => {
    this.setState({ modalVisible: !this.state.modalVisible });
  };

  _onCamera = () => {
    ImagePicker.openCamera({
      useFrontCamera: true,
      cropping: true
    })
      .then(image => {
        this._onModal();
        let temp = [];
        if (this.state.imageData.length != 0) {
          temp = this.state.imageData;
          temp.push(image);
        } else {
          temp.push(image);
        }
        this.setState({ imageData: temp });
      })
      .catch(e => {
        this._onModal();
      });
  };

  _onGallery = () => {
    ImagePicker.openPicker({
      multiple: true,
      minFiles: 1,
      maxFiles: 5,
      mediaType: "photo"
    })
      .then(images => {
        this._onModal();
        let temp = [];
        if (this.state.imageData.length != 0) {
          temp = this.state.imageData;
          for (let i = 0; i < images.length; i++) {
            temp.push(images[i]);
          }
        } else {
          temp = images;
        }
        this.setState({ imageData: temp });
      })
      .catch(e => {
        this._onModal();
      });
  };

  _onCancel = index => {
    var tempData = this.state.imageData;
    tempData.splice(index, 1);
    this.setState({ imageData: tempData });
  };

  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView />
        {/* ---------------------- Main Modal View ----------------------  */}

        <Modal
          isVisible={this.state.modalVisible}
          avoidKeyboard
          onBackButtonPress={this._onModal.bind(this)}
          onBackdropPress={this._onModal.bind(this)}
          supportedOrientations={["portrait"]}
        >
          <View style={[styles.mainModalView]}>
            <View
              style={{
                padding: 10,
                borderBottomColor: "#424242",
                borderBottomWidth: 0.5
              }}
            >
              <Text>Choose From</Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-around",
                padding: 20
              }}
            >
              <TouchableOpacity
                style={{ alignItems: "center" }}
                onPress={() => {
                  this._onCamera();
                }}
              >
                <View style={{ justifyContent: "center" }}>
                  <Image
                    source={require("./camera.png")}
                    resizeMode="contain"
                    style={{ height: 70, width: 100 }}
                  />
                </View>
                <Text> Camera </Text>
              </TouchableOpacity>

              <View
                style={{ borderLeftWidth: 1, borderLeftColor: "#283593" }}
              />

              <TouchableOpacity
                style={{ alignItems: "center" }}
                onPress={() => {
                  this._onGallery();
                }}
              >
                <View style={{ justifyContent: "center" }}>
                  <Image
                    source={require("./gallery.jpeg")}
                    resizeMode="contain"
                    style={{ height: 70, width: 100 }}
                  />
                </View>
                <Text> Gallery </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        {/* ---------------------- Selected images View ----------------------  */}

        <View
          style={{
            flex: 1
          }}
        >
          <FlatList
            enableEmptySections
            showsVerticalScrollIndicator={false}
            data={this.state.imageData}
            numColumns={2}
            contentContainerStyle={{}}
            renderItem={item => (
              <View>
                <TouchableOpacity
                  style={{
                    top: 5,
                    right: 5,
                    zIndex: 99,
                    margin: 5,
                    position: "absolute"
                  }}
                  onPress={() => {
                    this._onCancel(item.index);
                  }}
                >
                  <Image source={require("./close.png")} />
                </TouchableOpacity>
                <Image
                  style={{ height: 150, width: 150, margin: 5 }}
                  source={{ uri: item.item.path }}
                />
              </View>
            )}
          />
        </View>

        {/* ---------------------- Button View ----------------------  */}

        <View
          style={{
            flexDirection: "row",
            marginVertical: 10,
            width: "100%",
            justifyContent: "space-evenly"
          }}
        >
          <TouchableOpacity
            onPress={() => {
              this._onModal();
            }}
            style={styles.btn}
          >
            <Text style={styles.btntext}>Add Image</Text>
          </TouchableOpacity>

          {/* ========================= Mutation ========================== */}

          <Mutation mutation={uploadFileMutation}>
            {mutate => (
              <TouchableOpacity
                onPress={() => {
                  if (this.state.imageData.length != 0) {
                    var file = [];

                    for (let i = 0; i < this.state.imageData.length; i++) {
                      file[i] = new ReactNativeFile({
                        name:
                          Platform.OS === "ios"
                            ? "name." +
                              this.state.imageData[i].filename
                                .split(".")[1]
                                .toLowerCase()
                            : "name." +
                              this.state.imageData[i].mime.split("/")[1],
                        type:
                          Platform.OS === "ios"
                            ? "image/" +
                              this.state.imageData[i].filename
                                .split(".")[1]
                                .toLowerCase()
                            : this.state.imageData[i].mime,

                        uri: this.state.imageData[i].path
                      });
                    }

                    mutate({ variables: { file } })
                      .then(res => {
                        if (res.data.uploadFile == true) {
                          this.setState({ imageData: [] });
                          alert("Image Uploaded");
                        } else {
                          alert("Image not Uploaded, Please try again");
                        }
                      })
                      .catch(err => {
                        alert("Something wrong from server, Please try again");
                      });
                  } else {
                    alert("please Select image");
                  }
                }}
                style={styles.btn}
              >
                <Text style={styles.btntext}>Upload Image</Text>
              </TouchableOpacity>
            )}
          </Mutation>
        </View>

        <SafeAreaView />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: "center",
    alignItems: "center"
  },
  mainModalView: {
    width: "90%",
    backgroundColor: "#FFFFFF",
    alignSelf: "center",
    borderRadius: 10
  },
  btn: {
    backgroundColor: "#EDB600",
    alignItems: "center",
    width: "40%",
    padding: 15,
    borderRadius: 15
  },
  btntext: {
    color: "#FFFFFF"
  }
});

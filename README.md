# Upload Image in React Native

A code snippet for uploading pictures to Apollo Server


# React Native Code for Image Upload on ApolloGraphql

This ReactNative app code is using ro upload a image on Apollo Server using ApolloGraphql


## Steps for Installation & Run Project

```bash
1. brew install node
2. brew install watchman
3. npm install -g react-native-cli
4. cd project_directory
5. npm install
6. cd ios
7. pod install
8. cd ..
9. react-native run-ios( for ios)  or react-native run-android( for android)
```


## Configure Node Server

In project_directory/index.js file,

change uri to your server uri
const link = createUploadLink({ uri: "http://192.168.22.17:4000/" });

## Set Limit of Image selection

In project_directory/App.js file

```
......
......

_onGallery = () => {
    ImagePicker.openPicker({
    multiple: true,
    minFiles: 1, //change here (ios only)  
    maxFiles: 5, //change here (ios only)
    mediaType: "photo"
})
......
......

}
```
